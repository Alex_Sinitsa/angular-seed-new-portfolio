import { Component, OnInit } from '@angular/core';
// import { NameListService } from '../shared/index';
import { trigger, state, style, transition, animate } from '@angular/core';

/**
 * This class represents the lazy loaded catalogueComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-catalogue',
  templateUrl: 'catalogue.component.html',
  styleUrls: ['catalogue.component.css'],
})

export class CatalogueComponent implements OnInit {

  items: number[] = [];
  heightWindow: string;
  widthWindow: string;
  widthWindow05: string;
  timer: boolean;


  newName: string = '';
  errorMessage: string;
  names: any[] = [];

  /**
   * Creates an instance of the catalogueComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  // constructor(public nameListService: NameListService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    // this.getNames();
    this.getSizeWindow();

    for (let i=1; i<100; i++) {
      this.items.push(i);
    }
  }

  /**
   * Handle the nameListService observable
   */
  // getNames() {
  //   this.nameListService.get()
  //     .subscribe(
  //       names => this.names = names,
  //       error =>  this.errorMessage = <any>error
  //     );
  // }

  getSizeWindow() {
    let el = document.documentElement;
    this.widthWindow = el.clientWidth + 'px';
    this.widthWindow05 = parseFloat(this.widthWindow)/2 + 'px';
    this.heightWindow = el.clientHeight + 'px';

  }

  scrolling(e:any, el:HTMLElement) {
    let rect = el.getBoundingClientRect();
    let mL = rect.left;
    let mT = rect.top;
    let dif:number;
    let html = document.getElementsByTagName('body')[0];

    if(this.timer) return;

    html.classList.remove('animate');
    html.style.marginTop = mT + 'px';
    scrollBy(0, mT);
    el.classList.add('animate');
    

    setTimeout(() => {
      html.classList.add('animate');
      html.style.marginTop = '';
    }, 0);

    // отмена првторного нажатия 1500мс
    this.timer = true;
    setTimeout(() => {
      this.timer = false;
    }, 1500);

    // скрол вправо
    if ( e.target.classList.contains('scroll-right')) {
      if (e.target.nextElementSibling) {
        el.style.marginLeft = mL - parseFloat(this.widthWindow) +'px';
      } else if ((dif = parseInt(this.widthWindow) - el.lastElementChild.getBoundingClientRect().right ) > 0) {
        el.style.marginLeft = mL + dif + 'px';
      }
    }

    // скрол влево
    if ( e.target.classList.contains('scroll-left')) {
      if ((mL + parseFloat(this.widthWindow)) > 0) {
        el.style.marginLeft = '0px';
      } else {
      el.style.marginLeft = mL + parseFloat(this.widthWindow) +'px';
      }
    }
  }

  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  // addName(): boolean {
  //   // TODO: implement nameListService.post
  //   this.names.push(this.newName);
  //   this.newName = '';
  //   return false;
  // }

}
