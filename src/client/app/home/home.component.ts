import { Component, OnInit } from '@angular/core';
import { NameListService } from '../shared/index';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})
export class HomeComponent implements OnInit {

  newName: string = '';
  errorMessage: string;
  names: any[] = [];
///
  heightWindow: string;
  widthWindow: string;

  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public nameListService: NameListService) {}
  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getNames();
///
    this.getSizeWindow();
    // this.reSizeHome();
    // this.eventScroll();
  }
///



  // eventScroll() {
  //   let nav = document.getElementsByClassName('nav')[0];
  //   let parentNav = nav.parentElement;

  //   window.onscroll = function() {
  //     let bottomPar = parentNav.getBoundingClientRect().bottom;
  //     let bottomNav = nav.getBoundingClientRect().bottom;
  //     if (bottomPar < bottomNav) {
  //       nav.style.left = '160px';
  //       nav.style.opacity = '0';
  //     } else {
  //       nav.style.left = '';
  //       nav.style.opacity = '1';
  //     }
  //   };
  // }

  // handler(e: any) {
  //   let nav: HTMLElement;

  //   if (nav = e.target.classList.contains('nav')) {
  //     nav.style.left = '10px';
  //   }
  // }

  getSizeWindow() {
    let home = document.getElementById('home');
    let el = document.documentElement;

    this.widthWindow = el.clientWidth + 'px';
    this.heightWindow = el.clientHeight + 'px';
    home.style.height = this.heightWindow;
  }

  // reSizeHome() {
  //   // It’s bad
  //   let header = document.getElementsByClassName('wrapper')[0];
  //   let elA = document.getElementById('a');

  //   header.style.height = this.heightWindow;
  //   elA.style.left = '-160px';
  //   elA.style.opacity = '1';

  // }








///
  /**
   * Handle the nameListService observable
   */
  getNames() {
    this.nameListService.get()
      .subscribe(
        names => this.names = names,
        error => this.errorMessage = <any>error
      );
  }

  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  addName(): boolean {
    // TODO: implement nameListService.post
    this.names.push(this.newName);
    this.newName = '';
    return false;
  }

}
