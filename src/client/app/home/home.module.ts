import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { NameListService } from '../shared/name-list/index';

import { CatalogueModule } from './catalogue/catalogue.module';
import { HomeAboutModule } from './about/home.about.module';
import { MenuModule } from './menu/menu.module';
import { SliderModule } from './slider/slider.module';


@NgModule({
  imports: [CommonModule, SharedModule, CatalogueModule, HomeAboutModule, MenuModule, SliderModule],
  declarations: [HomeComponent],
  exports: [HomeComponent],
  providers: [NameListService]
})
export class HomeModule { }
