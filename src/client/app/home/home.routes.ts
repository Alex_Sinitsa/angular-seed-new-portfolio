import { Route } from '@angular/router';
import { HomeComponent } from './index';

import { CatalogueComponent } from './catalogue/index';

export const HomeRoutes: Route[] = [
  {
    path: '',
    component: HomeComponent
  }, {
    path: 'catalogue',
    component: CatalogueComponent
  }
];
