import { Component, OnInit } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.css']
})
export class MenuComponent implements OnInit {

  content: string;
  link: string;
  data: any[];
  active: any;
  i: number = 0;

  ngOnInit() {



    this.data = [
      {text: 'Описание услуги детейлинг', button: 'Подробнее о детейлинге', href: '#'
      , img: '/assets/svg/crown.svg', name: 'Детейлинг'},

      {text: 'Описание услуги покраска', button: 'Подробнее о покраске', href: '#'
      , img: '/assets/svg/pulverizator.svg', name: 'Покраска'},

      {text: 'Описание услуги палировка', button: 'Подробнее о палировке', href: '#'
      , img: '/assets/svg/grinder.svg', name: 'Полировка'},

      {text: 'Описание услуги и т.д.', button: 'Подробнее о детейлинге', href: '#'
      , img: '/assets/svg/crown.svg', name: 'Защитное покрытие'},

      {text: 'Описание услуги и т.д.', button: 'Подробнее о покраске', href: '#'
      , img: '/assets/svg/pulverizator.svg', name: 'Химчистка'},

      {text: 'Описание услуги и т.д.', button: 'Подробнее о полировке', href: '#'
      , img: '/assets/svg/grinder.svg', name: 'Защита интерьера'}
    ];
  }

  changeContent(el: any, i:number) {
    this.i = i;
    this.active = el;
    // this.oldEl.
    // this.content = this.data[id - 1].text;
    // this.link = this.data[id - 1].link;
    // this.oldEl = el;
  }
 }
