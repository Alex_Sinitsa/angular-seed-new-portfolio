import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-slider',
  templateUrl: 'slider.component.html',
  styleUrls: ['slider.component.css']
})
export class SliderComponent {
  content: any[] = [
    {text: 'asdfasdf asdfgas as adfgdfgad sdfhfgh sryw', img: '/assets/img/wheel.png'},
    {text: 'asdfasgfdgjgh   as adfgdfgad st tryw', img: '/assets/img/eng.png'},
    {text: 'asdfasdf asdfgas as agh  ghjghj st tryw', img: '/assets/img/rul.png'},
    {text: 'asdfasdf asdfgas as adfgdfgad st tryw', img: '/assets/img/front.png'},
    {text: 'asdfasg gh gas as adfgdfgad st tryw', img: '/assets/img/side.png'},
    {text: 'asdfasg gh gas as adfgdfgad st tryw', img: '/assets/img/light.png'}
  ];
  isActive: boolean = false;
  isHidden: boolean = false;
  oldSlide: Element;
  dotActive: Element;
  timer:    boolean;


  downDot(boolean: boolean, slide?: Element, dot?: Element) {
    if (this.timer) return;
    if (dot) this.dotActive = dot;
    if (this.oldSlide === slide) return;

    if (slide) {
      this.timer = true;
      slide.style.zIndex = 26;
      slide.classList.add('slider-animate');
      if (this.oldSlide) {
        this.oldSlide.classList.remove('slider-animate-rev');
        setTimeout(() => {
          this.oldSlide.classList.remove('slider-animate');
          this.oldSlide.style.top = '-1000px';
        }, 700);
      }
      setTimeout(() => {
        slide.style.zIndex = 10;
        this.oldSlide = slide;
        this.timer = false;
      }, 700);
    } else {
      if (this.oldSlide) {
        this.dotActive = null;
        this.oldSlide.classList.add('slider-animate-rev');
        setTimeout(() => {
          this.oldSlide.classList.remove('slider-animate');
          this.oldSlide.classList.remove('slider-animate-rev');
          this.oldSlide = null;
        }, 500);
      }
    }

    this.isActive = boolean;
  }

}
