import { Component, OnInit, SimpleChanges, ElementRef, ViewChild } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})


export class NavbarComponent implements OnInit {

  // @ViewChild('nav') nav: ElementRef;
  private menuBackShrink = false;


  ngOnInit() {
    // this.addEventListenerScroll();
  }


  // плавный скрол по якорях
  goTo(anchor: string ) {

    setTimeout(() => {
      let element: Element
        , body: Element
        , oldScroll: number
        , newScroll: number
        , dif: number;

      body = document.getElementsByTagName('sd-home')[0];
      body.classList.remove('animate');
      element = document.querySelector('#' + anchor);
      if (element) {
        oldScroll = window.pageYOffset;
        element.scrollIntoView();
        scrollBy(0, -86);
        newScroll = window.pageYOffset;
        dif = newScroll - oldScroll;
        body.style.marginTop = dif + 'px';

        setTimeout(() => {
          // element.classList.add('animate');
          body.classList.add('animate');
          body.style.marginTop = '0';
        }, 100);

      }
    }, 0);
  }


 // при прокрутке сокращет верхнюю панель навигации
  handler() {
    if (window.pageYOffset > 50) {
      this.menuBackShrink = true;
    } else {
      this.menuBackShrink = false;
    }
  }


}
